# CEE_690-02_EnvSpatialAnalysis

Repo to hold ansible script for installing app packages for the  
CEE_690-02_Environmental_Spatial_Data_Analysis VCM VM image

When provisioning VMs, VCM will  clone this repository to the VM and  
then run ./install.sh as the root user.

## install.sh
* __install.sh__ needs to be executable.
* __install.sh__ can run any type of shell commands.  The example provided kicks off an ansible playbook.
* You can update __/tmp/rapid\_image\_status.txt__ to provide status information to VCM

***_IMPORTANT:_** VCM will look for a file __/tmp/rapid\_image\_complete__ to know when the installation is complete.  
If that file is not found, **VCM will not mark the VM as complete**.

## Usage

```
./install.sh
```
